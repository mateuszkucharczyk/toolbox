#!/bin/bash
set -o errexit
set -o pipefail
set -o nounset
# using -e without -E will cause an ERR trap to not fire in certain scenarios
set -E

readonly SCRIPT_DIR="$( cd "$(dirname "$0")" ; pwd -P )";
readonly SCRIPT_NAME="${0##*/}";
readonly SCRIPT_PATH="${SCRIPT_DIR}/${SCRIPT_NAME}";
readonly LIB_PATH="${SCRIPT_DIR}/..";
source "${LIB_PATH}/install.config.sh";
source "${LIB_PATH}/install.source.sh";

readonly CNTLM_CONFIG='/etc/cntlm.conf';

function main() {
  local -r domain="${1:?[ERROR] domain not provided}";
  local -r user="${2:?[ERROR] user not provided}";
  local -r password="${3:?[ERROR] password not provided}";

  local hash_line;
  hash_line=$(echo ${password} | cntlm -H -u${user} -d${domain} | grep PassNTLMv2);
  
  if [ ! -f "${CNTLM_CONFIG}" ]; then
    echoerr "${CNTLM_CONFIG} not found!"
    return 1;
  fi
  
  replace_first_matching_line '\s*Domain\s.*' "Domain ${domain}" "${CNTLM_CONFIG}";
  replace_first_matching_line '\s*Username\s.*' "Username ${user}" "${CNTLM_CONFIG}";
  replace_first_matching_line '\s*PassNTLMv2\s.*' "${hash_line}" "${CNTLM_CONFIG}";
  service cntlm restart;
}

main "$@"
