#!/bin/bash
set -o errexit
set -o pipefail
set -o nounset
# using -e without -E will cause an ERR trap to not fire in certain scenarios
set -E

readonly SCRIPT_DIR="$( cd "$(dirname "$0")" ; pwd -P )";
readonly SCRIPT_NAME="${0##*/}";
readonly SCRIPT_PATH="${SCRIPT_DIR}/${SCRIPT_NAME}";
source "${SCRIPT_DIR}/../install.config.sh";
source "${SCRIPT_DIR}/../install.source.sh";

# readonly DEBUG='echo';
# readonly CONFIG='/dev/stdout';
readonly CONFIG='/etc/cntlm.conf';

function configureCntlm() {  
  local -r url="${1:?[ERROR] url not provided (http://<username>:<password>@<proxy>:<port>)}";
  local -r domain="${2:?[ERROR] domain not provided}";
  local noproxy="${3:-}";
  if [[ ! -z "${noproxy// }" ]]; then
    noproxy=", ${noproxy}";
  fi
  
  local -r schema=$(echo ${url} | cut -d ':' -f1);
  local -r usernamePasswordHostPort=$(echo ${url} | cut --delimiter=':' --fields=2- | cut --characters=3-);
  
  local -r usernamePassword=$(echo ${usernamePasswordHostPort} | cut --delimiter='@' --fields=-1 --only-delimited);
  local -r username=$(echo ${usernamePassword} | cut --delimiter=':' --fields=1);
  local -r password=$(echo ${usernamePassword} | cut --delimiter=':' --fields=2);
  
  local -r hostProxy=$(echo ${usernamePasswordHostPort} | cut --delimiter='@' --fields=2);
  local -r host=$(echo ${hostProxy} | cut --delimiter=':' --fields=1);
  local -r port=$(echo ${hostProxy} | cut --delimiter=':' --fields=2);
  
  if [ -f "${CONFIG}" ]; then
    $DEBUG mv -f "${CONFIG}" "${CONFIG}.backup";
  fi
  
  echo "Proxy   ${host}:${port}" >> "${CONFIG}";
  echo 'NoProxy   localhost, 127.0.0.*, 10.*'"${noproxy}" >> "${CONFIG}";
  echo "Listen    3128" >> "${CONFIG}";
  echo "Gateway yes" >> "${CONFIG}";
  echo "Allow       127.0.0.1" >> "${CONFIG}";
  echo "Allow       10.158.3.90" >> "${CONFIG}";
  echo "Allow       10.158.95.59" >> "${CONFIG}";
  echo "Allow       10.200.251.25" >> "${CONFIG}";
  echo "Allow       10.0.15.10" >> "${CONFIG}";
  echo "Allow       192.168.56.1" >> "${CONFIG}";

  if [ -n "${username}" ]; then
    echo "Username    ${username}" >> "${CONFIG}";
    echo "Domain      ${domain}" >> "${CONFIG}";
    echo "Auth        NTLMv2" >> "${CONFIG}"; 
    echo "PassNTLMv2  DUMMY" >> "${CONFIG}";
    $DEBUG ./configure-cntlm-password "${domain}" "${username}" "${password}"
  fi
}

function main() {
  apt-get update && \
  apt-get install -y cntlm && \
  service cntlm start && \
  configureCntlm "$@" && \
  service cntlm restart;
}

main "$@"
