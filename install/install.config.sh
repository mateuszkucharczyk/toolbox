#!/bin/bash

readonly DEBUG='';

readonly TARGET_USER="vagrant";
readonly TOOLBOX_PATH="/vagrant";
readonly CERTIFICATES_PATH="/vagrant/certificates";
readonly CONFIG_PATH="${TOOLBOX_PATH}/.config";
readonly PLUGINS_PATH="${CONFIG_PATH}/plugins";
