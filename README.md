# toolbox


## Troubleshooting
###VirtualBox high memory usage
| Description | Details |
| -------------: |:-------------|
| Ticket | [VirtualBox #19726](https://www.virtualbox.org/ticket/19726) |
| Host | MacOS Catalina 10.15.x (confirmed: 10.15.5, 10.15.7) |
| VirtualBox Version | 6.1.4 onward (still present in 6.1.22) |
| Problem | Doubled memory usage. Up until 6.1.2 memory and real memory were basically the same. Allocating 4GB in the VM resulted in a memory usage of 4GB. |
| Solution | None |
| Workaround | downgrade [VirtualBox to 6.1.2](https://download.virtualbox.org/virtualbox/6.1.2/VirtualBox-6.1.2-135662-OSX.dmg) |
---
###VirtualBox VM fails to start with ```rc=-1908``` error
| Description | Details           |
| -------------: |:-------------|
| Host | MacOS High Sierra 10.13 onward |
| VirtualBox Version | any |
| Problem | VirtualBox does not install properly and VM on start fails with error ```Kernel driver not installed (rc=-1908)``` |
| Root Cause | MacOS High Sierra 10.13 introduces a new feature [User-Approved Kernel Extension Loading] |
| Solution 1 | [Allow Oracle to install kernel extensions via System Preferences GUI] |
| Solution 2 | [Add Oracle to the list of approved developers in Recovery Mode via CMD] ```spctl kext-consent add VB5E2TV963``` |

[Allow Oracle to install kernel extensions via System Preferences GUI]: https://forums.virtualbox.org/viewtopic.php?f=8&t=84092#p399090
[Add Oracle to the list of approved developers in Recovery Mode via CMD]: https://forums.virtualbox.org/viewtopic.php?f=39&t=84778&start=15#p412500
[User-Approved Kernel Extension Loading]: https://developer.apple.com/library/archive/technotes/tn2459/_index.html
